with blt;

with Interfaces.C; use Interfaces.C;

procedure Main is
   Term: int;
   Key_Pressed: int;
begin
   Term := blt.terminal_open;
   blt.terminal_refresh;
   Key_Pressed := blt.terminal_read;
   blt.terminal_close;
end Main;
