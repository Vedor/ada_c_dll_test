with Interfaces.C; use Interfaces.C;

package blt is

   function terminal_open return int with
     Import => True,
     Convention => C,
     External_Name => "terminal_open";

   procedure terminal_close with
     Import => True,
     Convention => C,
     External_Name => "terminal_close";

   procedure terminal_refresh with
     Import => True,
     Convention => C,
     External_Name => "terminal_refresh";

   function terminal_read return int with
     Import => True,
     Convention => C,
     External_Name => "terminal_read";

end blt;
